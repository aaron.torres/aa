package com.softtek.academy.service;

import java.util.List;

import com.softtek.academy.entities.Momentum;

public interface MomentumService {
	public abstract Momentum AddToTheDb(Momentum momentum); //just for testing
	public abstract Double obtainMonthHours(String name,String mes); //Punto #1
	public abstract List<Momentum> getMomentumbyDate(String name, String startDate, String EndDate); //punto #2
	public abstract List<Momentum> getMomentumByMonth(String month); //punto #3
	public abstract List<Momentum> getMomentumsbyDate(String startDate, String EndDate); //punto #2
	public abstract Object listAllMomentums();
}

