package com.softtek.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalTestJmlsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalTestJmlsApplication.class, args);
	}

}
