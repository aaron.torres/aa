package com.softtek.academy.repositories;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.softtek.academy.entities.Momentum;


@Repository("momentumJpaRepository")
public interface MomentumJpaRepository  extends JpaRepository<Momentum, Serializable>{

	public abstract Momentum findById(int id);
	
	public abstract Momentum findByName(String name);
	
	public abstract Momentum findByIdAndName(int id,String name);
	
}
