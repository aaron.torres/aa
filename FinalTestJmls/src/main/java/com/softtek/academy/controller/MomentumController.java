package com.softtek.academy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import com.softtek.academy.entities.Momentum;
import com.softtek.academy.service.MomentumService;


@Controller
@RequestMapping("/api/v1")
public class MomentumController {
	
	@Autowired
	@Qualifier("momentumServiceImpl")
	private MomentumService momentumService;
	
	@GetMapping("/")
	public RedirectView redirect() {
		return new RedirectView("/api/v1/showform"); 
	}
	
	@GetMapping("/showform")
	public String showForm(Model model) {
		model.addAttribute("momentum",new Momentum()); 
		return "form";
	}
	
	@PostMapping("/addmomentum")
	public ModelAndView addmomentum(@ModelAttribute("momentum") Momentum momentum ) {
		ModelAndView mav=new ModelAndView("result");
		mav.addObject("momentum",momentum);
		return mav;
	}
	
	@GetMapping("/listAll") //para obtener todos los momentums
	public ModelAndView getAllCourses() {
		ModelAndView mav=new ModelAndView("showMomentums");
		mav.addObject("momentums",momentumService.listAllMomentums());
		return mav;
	}
}
