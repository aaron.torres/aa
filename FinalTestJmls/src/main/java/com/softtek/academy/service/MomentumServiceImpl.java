package com.softtek.academy.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.softtek.academy.entities.Momentum;
import com.softtek.academy.repositories.MomentumJpaRepository;

@Service("momentumServiceImpl")
public class MomentumServiceImpl implements MomentumService {

	@Autowired
	@Qualifier("momentumJpaRepository")
	MomentumJpaRepository momentumJpaRepository;
	
	@Override
	public Double obtainMonthHours(String name, String mes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Momentum> getMomentumbyDate(String name, String startDate, String EndDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Momentum> getMomentumByMonth(String month) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Momentum> getMomentumsbyDate(String startDate, String EndDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Momentum AddToTheDb(Momentum momentum) { //Test to see if everything is ok
		return momentumJpaRepository.save(momentum);
	}

	@Override
	public Object listAllMomentums() {
		return momentumJpaRepository.findAll();
	}

}
